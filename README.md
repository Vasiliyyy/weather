
# Weather Quick Start 


### Quick start
**Make sure you have Node version >= 6.0 and NPM >= 3**

```bash
# clone repo
git clone https://bitbucket.org/Vasiliyyy/weather.git

# change directory to our repo
cd weather

# install the repo with npm
npm install

# start the server
npm start

# use Hot Module Replacement
npm run server:dev:hmr

```
go to [http://0.0.0.0:3000](http://0.0.0.0:3000) or [http://localhost:3000](http://localhost:3000) in your browser