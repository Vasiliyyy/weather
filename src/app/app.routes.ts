import { Routes } from '@angular/router';
import { DataResolver } from './app.resolver';
import {HomePageComponent} from './pages/home/home.page';

export const ROUTES: Routes = [
  { path: '',      component: HomePageComponent },
];
