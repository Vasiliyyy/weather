import { Component } from '@angular/core';
import { TimeService } from './time.service';
import { ITimeFormat } from './time-format.interface';

@Component({
  selector: 'time-now',
  templateUrl: 'time.component.html',
  styleUrls: ['time.component.scss'],
  providers: [TimeService]
})

export class TimeComponent {
  private currentTime: ITimeFormat;

  constructor(public timeService: TimeService) {
    this.currentTime = timeService.getCurrentTime();
    let t = this;
    setInterval(function () {
      t.currentTime = timeService.getCurrentTime();
    }, 1000);
  }
}
