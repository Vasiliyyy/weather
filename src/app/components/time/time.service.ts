import { Injectable } from '@angular/core';
import { ITimeFormat } from './time-format.interface';

@Injectable()
export class TimeService {
  private currentDate: Date = new Date();
  private monthName: string = '';
  private dayNumber: string = '';
  private dayName: string = '';
  private hours: string = '';
  private minutes: string = '';
  private AmPm: boolean = false;
  private monthNames: string[] = ['January', 'February', 'March', 'April', 'May', 'June',
    'July', 'August', 'September', 'October', 'November', 'December'];
  private dayNames: string[] = [ 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

  public getCurrentTime(): ITimeFormat {
    this.currentDate = new Date();
    this.monthName = this.monthNames[this.currentDate.getMonth()];
    this.dayNumber = ('0' + this.currentDate.getDate()).slice(-2);
    this.dayName = this.dayNames[this.currentDate.getDay()];
    this.hours = ('0' + this.currentDate.getHours()).slice(-2);
    this.minutes = ('0' + this.currentDate.getMinutes()).slice(-2);
    this.AmPm = this.isAM(this.currentDate.getHours());

    return {monthName: this.monthName, dayNumber: this.dayNumber, dayName: this.dayName, hours: this.hours, minutes: this.minutes, isAM: this.AmPm};
  }

  private isAM(hour): boolean{
    return hour <= 12;
  }
}
