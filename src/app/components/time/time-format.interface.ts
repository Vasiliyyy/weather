export interface ITimeFormat {
  monthName: string;
  dayNumber: string;
  dayName: string;
  hours: string;
  minutes: string;
  isAM: boolean;
}
