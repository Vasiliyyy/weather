import { Component, Input } from '@angular/core';
import { IWeatherBase } from '../../interfaces/weather-base.interface';

@Component({
  selector: 'temperature',
  templateUrl: 'temperature.component.html',
  styleUrls: ['temperature.component.scss']
})

export class TemperatureComponent {
  @Input() currentWeather: IWeatherBase;
}
