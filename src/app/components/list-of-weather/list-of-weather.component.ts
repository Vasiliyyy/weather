import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {TimeService} from '../time/time.service';
import {IAppState} from '../../services/app-state/app-state.interface';
import {IWeatherList} from '../../interfaces/weather-list.interface';
import {Subscription} from 'rxjs/Subscription';
import {WeatherService} from '../../services/weather/weather.service';

@Component({
  selector: 'list-of-weather',
  templateUrl: 'list-of-weather.component.html',
  styleUrls: ['list-of-weather.component.scss'],
  providers: [TimeService]
})

export class ListOfWeatherComponent implements OnInit, OnDestroy {
  @Input() appState: IAppState = <IAppState> {};
  private _weatherList: IWeatherList;
  private _subscriptionWeatherList: Subscription;

  constructor(private weatherService: WeatherService) {
  }

  public ngOnInit() {
    this._subscriptionWeatherList = this.weatherService.weatherList$
      .subscribe((weatherList: IWeatherList) => {
      this._weatherList = weatherList;
    });
  }

  public getHourlyWeather() {
    if (!this.appState.isHourly) {
      this.weatherService.changeTypeOfPeriod(true);
    }
  }

  public getWeekWeather() {
    if (this.appState.isHourly) {
    this.weatherService.changeTypeOfPeriod(false);
    }
  }

  public changeCurrentPeriod(val: number) {
    this.weatherService.changePeriod(val);
  }

  public ngOnDestroy() {
    this._subscriptionWeatherList.unsubscribe();
  }

}
