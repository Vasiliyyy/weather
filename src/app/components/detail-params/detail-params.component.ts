import { Component, Input } from '@angular/core';
import { IWeatherBase } from '../../interfaces/weather-base.interface';

@Component({
  selector: 'detail-params',
  templateUrl: 'detail-params.component.html',
  styleUrls: ['detail-params.component.scss']
})

export class DetailParamsComponent {
 @Input()
  private currentWeather: IWeatherBase;
}
