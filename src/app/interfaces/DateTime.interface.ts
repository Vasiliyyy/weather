export interface IDateTime {
  date: string;
  time: string;
}
