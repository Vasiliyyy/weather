import { ILocation } from '../components/geolocation/interfaces/location.interface';
import { IWeatherSky } from '../services/weather/interfaces/weather-sky.interface';
import { ITemperature } from './temperature.interface';
import { IDateTime } from './DateTime.interface';
import { IWind } from './wind.interface';
import { ISun } from './sun.interface';

export interface IWeatherBase {
  city: string;
  timezone: string;
  date: IDateTime;
  temp: ITemperature;
  sky: IWeatherSky;
  sun: ISun;
  station: string;
  wind: IWind;
  location: ILocation;
  visibility: number;
}
