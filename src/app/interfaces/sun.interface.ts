export interface ISun {
  sunrise: string;
  sunset: string;
}
