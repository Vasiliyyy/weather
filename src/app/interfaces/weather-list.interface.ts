import { IDateTime } from './DateTime.interface';
import { ITemperature } from './temperature.interface';
import { IWeatherSky } from '../services/weather/interfaces/weather-sky.interface';

export interface IWeatherList {
  data: Array<{
    date: IDateTime;
    temp: ITemperature;
    sky: IWeatherSky;
  }>;
}
