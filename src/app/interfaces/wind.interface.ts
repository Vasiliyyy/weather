export interface IWind {
  direction: number;
  speed: number;
}
