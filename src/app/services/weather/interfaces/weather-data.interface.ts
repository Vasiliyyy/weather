import { IWeatherSky } from './weather-sky.interface';

export interface IWeatherData {
  wind_cdir: string;
  rh: number;
  wind_spd: number;
  pop: number;
  wind_cdir_full: string;
  app_temp: number;
  pres: number;
  dewpt: number;
  snow: number;
  uv: number;
  wind_dir: number;
  weather: IWeatherSky;
  ts: number;
  precip: number;
  datetime: string;
  temp: number;
  slp: number;
  clouds: number;
  vis: number;
}
