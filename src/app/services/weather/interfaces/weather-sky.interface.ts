export interface IWeatherSky {
  icon: string;
  code: string;
  description: string;
}
