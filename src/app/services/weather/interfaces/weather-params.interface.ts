import { IWeatherData } from './weather-data.interface';

export interface IWeatherParams {
  lat: number;
  lon: number;
  city_name: string;
  data: IWeatherData[];
  timezone: string;
  country_code: string;
  state_code: string;
}
