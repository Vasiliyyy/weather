import {Injectable, OnDestroy} from '@angular/core';
import {WeatherApiConfigService} from './config.service';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import {Http} from '@angular/http';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {IWeatherParams} from './interfaces/weather-params.interface';
import {ILocation} from '../../components/geolocation/interfaces/location.interface';
import {IAppState} from '../app-state/app-state.interface';
import {Subscription} from 'rxjs/Subscription';
import {AppStateService} from '../app-state/app-state.service';
import {IWeatherBase} from '../../interfaces/weather-base.interface';
import {IWeatherList} from '../../interfaces/weather-list.interface';

@Injectable()
export class WeatherService implements OnDestroy {
  private key: string;

  private _weatherResponse: IWeatherParams;

  private weatherBase: IWeatherBase;
  private weatherList: IWeatherList;

  private _appState: IAppState;
  private _subscriptionAppState: Subscription;

  private _selectedWeather: BehaviorSubject<IWeatherBase> = new BehaviorSubject<IWeatherBase>(null);
  public selectedWeather$ = this._selectedWeather.asObservable();

  private _weatherList: BehaviorSubject<IWeatherList> = new BehaviorSubject<IWeatherList>(null);
  public weatherList$ = this._weatherList.asObservable();

  constructor(private configService: WeatherApiConfigService,
              private http: Http,
              private appStateService: AppStateService) {
    this.key = configService.apiKey;
    this._subscriptionAppState = this.appStateService.currentState$
      .subscribe((appState: IAppState) => {
        this._appState = appState;
      });
  }

  public changeLocation(location: ILocation) {
    this.appStateService.selectLocation(location);
    this.getWeather(this._appState.location, this._appState.isHourly).then();
  }

  public changePeriod(key: number) {
    this.appStateService.selectPeriod(key);
    this.initSelectedWeather(this._weatherResponse, this._appState.selectedPeriod);
  }

  public changeTypeOfPeriod(isHourly: boolean) {
    this.appStateService.selectIsHourly(isHourly);
    this.getWeather(this._appState.location, this._appState.isHourly).then();
  }

  public ngOnDestroy() {
    this._subscriptionAppState.unsubscribe();
  }

  private initSelectedWeather(data: IWeatherParams, key: number) {
    this.weatherBase = <IWeatherBase>{
      city: data.city_name,
      timezone: data.timezone,
      date: {
        date: data.data[key].datetime.split(':')[0],
        time: data.data[key].datetime.split(':')[1]
      },
      temp: {
        temp: data.data[key].temp,
        tempFilling: data.data[key].app_temp
      },
      sky: data.data[key].weather,
      wind: {
        direction: data.data[key].wind_dir,
        speed: data.data[key].wind_spd
      },
      location: {
        lat: data.lat,
        long: data.lon
      }
    };
    this._selectedWeather.next(this.weatherBase);
  }

  private initWeatherList(data: IWeatherParams) {
    this.weatherList = <IWeatherList>{};
    this.weatherList.data = [];
    for (let i = 0; i < data.data.length && i < 8; i++) {
      this.weatherList.data.push({
        date: {
          date: data.data[i].datetime.split(':')[0],
          time: data.data[i].datetime.split(':')[1]
        },
        temp: {
          temp: data.data[i].temp,
          tempFilling: data.data[i].app_temp
        },
        sky: {
          icon: data.data[i].weather.icon,
          code: data.data[i].weather.code,
          description: data.data[i].weather.description
        }
      });
    }
    this._weatherList.next(this.weatherList);
    console.log(this.weatherList);
  }

  private getWeather(location: ILocation, isHourly: boolean) {
    switch (isHourly) {
      case true: {
        return this.http.get(this.configService.getApiUrl() + 'forecast/3hourly?lat=' + location.lat + '&lon=' + location.long + '&key=' + this.key)
          .map((response) => response.json())
          .toPromise()
          .then((data) => {
            this._weatherResponse = data;
            this.initSelectedWeather(data, this._appState.selectedPeriod);
            this.initWeatherList(data);
          });
      }
      case false: {
        return this.http.get(this.configService.getApiUrl() + 'forecast/daily?lat=' + location.lat + '&lon=' + location.long + '&key=' + this.key)
          .map((response) => response.json())
          .toPromise()
          .then((data) => {
            this._weatherResponse = data;
            this.initSelectedWeather(data, this._appState.selectedPeriod);
            this.initWeatherList(data);
          });
      }
      default: {
        break;
      }
    }
  }
}
