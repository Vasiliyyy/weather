export class WeatherApiConfigService {
  public apiKey: string = 'f91dcb9ef503449a8761919919594e7e';

  public getApiUrl(): string {
    return 'https://api.weatherbit.io/v1.0/';
  }

}
