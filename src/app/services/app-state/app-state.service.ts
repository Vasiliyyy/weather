import { Injectable } from '@angular/core';
import { IAppState } from './app-state.interface';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { ILocation } from '../../components/geolocation/interfaces/location.interface';

@Injectable()
export class AppStateService {
  private _currentState: BehaviorSubject<IAppState> = new BehaviorSubject<IAppState>(null);
  public currentState$ = this._currentState.asObservable();

  private appState: IAppState = <IAppState>{
    isHourly: true,
    selectedPeriod: 0,
    location:{
      lat: 42,
      long: 20,
    }
  };

  constructor() {
    this._currentState.next(this.appState);
  }

  public selectPeriod(period: number) {
    this.appState.selectedPeriod = period;
    this._currentState.next(this.appState);
  }

  public selectIsHourly(isHourly: boolean){
    this.appState.isHourly = isHourly;
    this._currentState.next(this.appState);
  }

  public selectLocation(location: ILocation){
    this.appState.location = location;
    this._currentState.next(this.appState);
  }

}
