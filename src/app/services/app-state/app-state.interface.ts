import { ILocation } from '../../components/geolocation/interfaces/location.interface';

export interface IAppState {
  isHourly: boolean;
  selectedPeriod: number;
  location: ILocation;
}
