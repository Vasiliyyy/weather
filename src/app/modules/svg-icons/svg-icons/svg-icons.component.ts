import { Component, Input, OnInit, ViewChild, ElementRef, ViewEncapsulation, OnChanges } from '@angular/core';
import { SvgIconsService } from './svg-icons.service';

@Component({
  selector: 'svg-icons',
  styleUrls: ['./svg-icons.component.scss'],
  encapsulation: ViewEncapsulation.None,
  templateUrl: 'svg-icons.component.html',
  providers: [SvgIconsService]
})

export class SvgIconsComponent implements OnInit, OnChanges {
  private _name: string = 'sun';

  @ViewChild('dataContainer') public dataContainer: ElementRef;

  @Input() code: string;

  constructor(private svgIconsService: SvgIconsService) {
  }

  public ngOnInit() {
    this.dataContainer.nativeElement.innerHTML = this.svgIconsService.getSvgBodyByName(this._name);
  }

  public ngOnChanges() {
    let code = parseInt(this.code);
    this._name = this.getIconNameByCode(code);
    this.dataContainer.nativeElement.innerHTML = this.svgIconsService.getSvgBodyByName(this._name);
  }

  private getIconNameByCode(code: number): string {
    if (code >= 800 && code <= 801) {
      return 'sun';
    }
    else if (code >= 802 && code <= 803) {
      return 'half-cloudy';
    }
    else if (code >= 700 && code <= 751 || code == 804) {
      return 'cloudy';
    }
    else if (code == 900 || code >= 300 && code <= 302 || code >= 500 && code <= 623) {
      return 'rain';
    }
    else if (code >= 200 && code <= 233) {
      return 'storm';
    }
  }
}
