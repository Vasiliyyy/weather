import { Component, ElementRef, Input, OnChanges, OnInit, ViewChild } from '@angular/core';
import { BackgroundImageService } from './background-image.service';

@Component({
  selector: 'background',
  templateUrl: 'background-image.component.html',
  styleUrls: ['background-image.component.scss'],
  providers: [BackgroundImageService]
})

export class BackgroundImagesComponent implements OnInit, OnChanges {
  public animate: string = '';
  @ViewChild('dataContainer') dataContainer: ElementRef;
  @Input() code: string;
  private _name: string = 'sun';
  private backgroundUrl: string;

  constructor(private backgroundImageService: BackgroundImageService) {
  }

  public ngOnInit() {
    this.backgroundUrl = this.backgroundImageService.getImageByName(this._name);
  }

  public ngOnChanges() {
    let code = parseInt(this.code);
    this._name = this.getImageNameByCode(code);
    this.backgroundUrl = this.backgroundImageService.getImageByName(this._name);
  }

  private getImageNameByCode(code: number): string {
    if (code >= 800 && code <= 801) {
      return 'sun';
    }else if (code >= 802 && code <= 803) {
      return 'half-cloudy';
    }else if (code >= 700 && code <= 751 || code === 804) {
      return 'cloudy';
    }else if (code === 900 || code >= 300 && code <= 302 || code >= 500 && code <= 623) {
      return 'rain';
    }else if (code >= 200 && code <= 233) {
      return 'storm';
    }else {
      return 'sun';
    }
  }
}
