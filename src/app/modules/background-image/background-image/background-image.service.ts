import { Injectable } from '@angular/core';

@Injectable()
export class BackgroundImageService {
  public getImageByName(name): string {
    let allImages = this.getAllImages();
    return (name) ? allImages[name] : {};
  }

  private getAllImages(): Object {
    return {
      'sun': `./assets/sky-img/sun.jpg`,
      'half-cloudy': `./assets/sky-img/half-cloudy.jpg`,
      'cloudy': `./assets/sky-img/cloudy.jpg`,
      'rain': `./assets/sky-img/rain.jpg`,
      'storm': `./assets/sky-img/storm.jpg`,
    };
  }
}
