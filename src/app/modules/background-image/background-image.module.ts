import { NgModule } from '@angular/core';
import { BackgroundImagesComponent } from './background-image/background-image.component';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [
    BackgroundImagesComponent
  ],
  exports: [
    BackgroundImagesComponent
  ],
  imports: [
    CommonModule
  ],
})

export class BackgroundImageModule {
}
