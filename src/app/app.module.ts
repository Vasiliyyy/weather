import { BrowserModule } from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpModule } from '@angular/http';
import {
  NgModule,
  ApplicationRef
} from '@angular/core';
import {
  removeNgStyles,
  createNewHosts,
  createInputTransfer
} from '@angularclass/hmr';
import {
  RouterModule,
  PreloadAllModules
} from '@angular/router';

/*
 * Platform and Environment providers/directives/pipes
 */
import { ENV_PROVIDERS } from './environment';
import { ROUTES } from './app.routes';
// App is our top level component
import { AppComponent } from './app.component';
import { APP_RESOLVER_PROVIDERS } from './app.resolver';
import { AppState, InternalStateType } from './app.service';


import '../styles/styles.scss';
import '../styles/headings.css';
import { HomePageComponent } from './pages/home/home.page';
import { WeatherService } from './services/weather/weather.service';
import { WeatherApiConfigService } from './services/weather/config.service';
import { BackgroundImageModule } from './modules/background-image/background-image.module';
import { SvgIconsModule } from './modules/svg-icons/svg-icons.module';
import { ListOfWeatherComponent } from './components/list-of-weather/list-of-weather.component';
import { TimeComponent } from './components/time/time.component';
import { TemperatureComponent } from './components/temperature/temperature.component';
import { DetailParamsComponent } from './components/detail-params/detail-params.component';
import {AgmCoreModule} from "@agm/core";
import {GeolocationComponent} from "./components/geolocation/geolocation.component";
import {AppStateService} from "./services/app-state/app-state.service";

// Application wide providers
const APP_PROVIDERS = [
  ...APP_RESOLVER_PROVIDERS,
  AppState
];

type StoreType = {
  state: InternalStateType,
  restoreInputValues: () => void,
  disposeOldHosts: () => void
};

/**
 * `AppModule` is the main entry point into Angular2's bootstraping process
 */
@NgModule({
  bootstrap: [ AppComponent ],
  declarations: [
    AppComponent,
    HomePageComponent,
    ListOfWeatherComponent,
    TimeComponent,
    TemperatureComponent,
    DetailParamsComponent,
    GeolocationComponent
  ],
  /**
   * Import Angular's modules.
   */
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(ROUTES, { useHash: true, preloadingStrategy: PreloadAllModules }),
    BackgroundImageModule,
    SvgIconsModule,
    AgmCoreModule.forRoot({
      apiKey: "AIzaSyAnwcXXNCi-wElSkQZb0XKgG3Ay-Lh7VW8",
      libraries: ["places"]
    }),
    ReactiveFormsModule
  ],
  /**
   * Expose our Services and Providers into Angular's dependency injection.
   */
  providers: [
    ENV_PROVIDERS,
    APP_PROVIDERS,
    WeatherService,
    WeatherApiConfigService,
    AppStateService
  ]
})
export class AppModule {

  constructor(
    public appRef: ApplicationRef,
    public appState: AppState
  ) {}

  public hmrOnInit(store: StoreType) {
    if (!store || !store.state) {
      return;
    }
    console.log('HMR store', JSON.stringify(store, null, 2));
    /**
     * Set state
     */
    this.appState._state = store.state;
    /**
     * Set input values
     */
    if ('restoreInputValues' in store) {
      let restoreInputValues = store.restoreInputValues;
      setTimeout(restoreInputValues);
    }

    this.appRef.tick();
    delete store.state;
    delete store.restoreInputValues;
  }

  public hmrOnDestroy(store: StoreType) {
    const cmpLocation = this.appRef.components.map((cmp) => cmp.location.nativeElement);
    /**
     * Save state
     */
    const state = this.appState._state;
    store.state = state;
    /**
     * Recreate root elements
     */
    store.disposeOldHosts = createNewHosts(cmpLocation);
    /**
     * Save input values
     */
    store.restoreInputValues  = createInputTransfer();
    /**
     * Remove styles
     */
    removeNgStyles();
  }

  public hmrAfterDestroy(store: StoreType) {
    /**
     * Display new elements
     */
    store.disposeOldHosts();
    delete store.disposeOldHosts;
  }

}
