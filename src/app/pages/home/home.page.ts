import { Component, OnDestroy, OnInit } from '@angular/core';
import { WeatherService } from '../../services/weather/weather.service';
import { Subscription } from 'rxjs/Subscription';
import { AppStateService } from '../../services/app-state/app-state.service';
import { IAppState } from '../../services/app-state/app-state.interface';
import { IWeatherBase } from '../../interfaces/weather-base.interface';

@Component({
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss']
})

export class HomePageComponent implements OnInit, OnDestroy {
  private _selectedWeather: IWeatherBase;
  private _subscriptionSelectedWeather: Subscription;

  private _appState: IAppState;
  private _subscriptionAppState: Subscription;

  constructor(private weatherService: WeatherService, private appStateService: AppStateService) {

  }

  public ngOnInit() {
    this._subscriptionAppState = this.appStateService.currentState$
      .subscribe((appState: IAppState) => {
        this._appState = appState;
      });
    this._subscriptionSelectedWeather = this.weatherService.selectedWeather$
      .subscribe((selectedWeather: IWeatherBase) => {
        this._selectedWeather = selectedWeather;
      });
  }

  public ngOnDestroy() {
    this._subscriptionSelectedWeather.unsubscribe();
    this._subscriptionAppState.unsubscribe();
  }
}
